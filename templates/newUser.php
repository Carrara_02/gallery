<?php
require('../templates/header.php');
require('../templates/menu.php');
?>
    <div class="container">
        <form method="post" action="index.php?action=save-user">
            <h3>Registra nuovo utente</h3>
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" name="username">
            </div>
            <div class="mb-3">
                <label for="newPassword_second" class="form-label">Password</label>
                <input type="password" class="form-control" id="newPassword" name="password">
            </div>
            <div class="mb-3">
                <label for="newPassword_second" class="form-label">Ripeti la password</label>
                <input type="password" class="form-control" id="newPasswordRepeat" name="Repeatpassword">
            </div>
            <div>
                <input class="form-check-input" type="checkbox" value="0" id="isAdmin" name="isAdmin">
                <label for="username" class="form-label">Is Admin</label>
            </div>
            <div class="d-flex">
                <button type="submit" class="btn btn-primary">Registra</button>
                <?php
                if (isset($_SESSION['messaggio'])) {
                    ?>
                    <div class="text-danger my-auto ms-3"><?= $_SESSION['messaggio']; ?></div>
                    <?php
                }
                ?>
            </div>
        </form>
    </div>

<?php
require('../templates/footer.php');
?>
