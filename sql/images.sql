CREATE TABLE `images`
(
    `id`       INT          NOT NULL AUTO_INCREMENT,
    `filename` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    UNIQUE KEY `id` (`id`) USING BTREE,
    UNIQUE KEY `filename` (`filename`) USING BTREE
);
