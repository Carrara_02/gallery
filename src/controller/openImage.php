<?php
require('../templates/header.php');
require('../templates/menu.php');

    function openImage(){
        $titolo = $_REQUEST['title'];
        $fileName = $_REQUEST['fileName'];

        ?>
        <div class="container text-center">
            <h2><?php echo $titolo?></h2>
            <img src="<?php echo $fileName?>" alt="<?php echo $titolo?>" style="height:120%;width:75%;">

            <form class="text-center" method="post" action="index.php?action=images-list" style="margin: 1%">
                <button class="btn btn-primary btn-sm">Indietro</button>
            </form>
        </div>
        <?php
    }

?>

