<?php
require('../templates/header.php');
require('../templates/menu.php');
?>
    <div class="container">
        <?php
            global $db;
            $sql = "SELECT * FROM images ;";
            $rs = $db->execute($sql);
        ?>
        <form method="post" action="index.php?action=add-image" style="margin-left: 20%">
            <input type="submit" class="btn btn-success" value="AGGIUNGI">
        </form>
        <?php
        foreach ($rs as $result) {?>
        <div class="container justify-content-center text-center col">
            <div class="container justify-content-center text-center col">
                <h3 class=""><?php echo $result['title']?></h3>
                <img class="rounded justify-content-center" src="<?php echo "./upload/".$result['filename']?>" style="height:225px;width:450px;margin:2px">
                <br>
                <div class="container d-flex justify-content-center">
                    <form action="index.php?action=open-image" method="post">
                        <button class="btn btn-success" style="margin:1px;">Apri</button>
                        <input type="hidden" value="<?php echo "./upload/".$result['filename']?>" name="fileName" id="fileName">
                        <input type="hidden" value="<?php echo $result['title']?>" name="title" id="title">
                    </form>
                    <form action="index.php?action=edit-image" method="post">
                        <?php
                        if($_SESSION["isAdmin"]){
                            ?>
                            <button class="btn btn-primary" style="margin:1px;">Modifica</button>
                            <?php
                        }
                        ?>
                        <input type="hidden" value="<?php echo "./upload/".$result['filename']?>" name="fileName" id="fileName">
                        <input type="hidden" value="<?php echo $result['title']?>" name="title" id="title">
                        <input type="hidden" value="<?php echo $result['id']?>" name="id" id="id">
                    </form>
                    <br><br><br><br>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
require('../templates/footer.php');
?>
