<?php 
require('../templates/header.php');
require('../templates/menu.php');
?>
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Username</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
            <?php
                global $db;
                $sql = "SELECT * FROM app_user ;";
                $rs = $db->execute($sql);
                foreach ($rs as $result) {
                    ?>
                    <tr>
                        <th><?php echo $result['id'] ?></th>
                        <td><?php echo $result['username'] ?></td>
                        <td>
                            <form method="post" action="index.php?action=modify-user">
                                <input type="hidden" value="<?php echo $result['id'];?>" name='id' id="id">
                                <button class="btn btn-primary btn-sm">Modifica</button>
                            </form>
                        </td>
                        <td>
                            <form method="post" action="index.php?action=delete-user">
                                <input type="hidden" value="<?php echo $result['id'];?>" name='id' id="id">
                                <button class="btn btn-danger btn-sm">Cancella</button>
                            </form>
                        </td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
    <a href="index.php?action=add-user" class="btn btn-success btn-sm">Aggiungi</a>
</div>
<?php
require('../templates/footer.php');
?>