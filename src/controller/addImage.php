<?php
require('../templates/header.php');
require('../templates/menu.php');
?>
    <div class="container">
        <form action="index.php?action=save-image" method="post" enctype="multipart/form-data">
            <h3>Aggiungi immagine</h3>
            <div class="mb-3">
                <label for="title" class="form-label">Titolo</label>
                <input type="text" class="form-control" id="title" aria-describedby="usernameHelp" name="title">
            </div>
            <div class="mb-3">
                <label for="upload5">
                    <br>
                    <input type="file" id="fileName" name="fileName" accept="image/png, image/jpeg">
                </label>
            </div>
            <div class="d-flex">
                <button type="submit" class="btn btn-primary">Aggiungi</button>
            </div>
        </form>
    </div>

<?php
require('../templates/footer.php');
?>