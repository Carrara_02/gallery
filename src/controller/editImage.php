<?php
require('../templates/header.php');
require('../templates/menu.php');

$filename = $_REQUEST['fileName'];
$title = $_REQUEST['title'];
$id = $_REQUEST['id'];
?>
    <div class="container text-center">
        <form action="index.php?action=upadte-image" method="post">
            <div class="mb-3">
                <label for="newPassword_second" class="form-label">ID</label>
                <input type="text" name="id" class="form-control" id="id" value="<?php echo $id?>" readonly="">
            </div>
            <div class="mb-3">
                <label for="newPassword_second" class="form-label">Title</label>
                <input type="text" name="title" class="form-control" id="title" value="<?php echo $title?>">
            </div>
            <img src="<?php echo $filename?>" style="height:50%;width:25%;">
            <br>
            <input type="submit" class="btn btn-primary" style="margin:1px;" value="Aggiorna">
        </form>
        <form action="index.php?action=delete-image" method="post">
            <input type="submit" class="btn btn-danger" style="margin:1px;" value="Elimina">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id?>">
            <input type="hidden" name="nomeFile" class="form-control" id="nomeFile" value="<?php echo $filename?>">
        </form>
    </div>
<?php

?>
