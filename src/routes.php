<?php
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'homepage';

switch ($action){
    case 'homepage':
        require('../src/controller/homepage.php');
        break;
    case 'login':
        require('../src/controller/Access.php');
        login();
        break;
    case 'login-check':
        require('../src/controller/Access.php');
        loginCheck();
        break;
    case 'logout':
        require('../src/controller/Access.php');
        logout();
        break;
    case 'change-password':
        require('../src/controller/Password.php');
        showForm();
        break;
    case 'set-password':
        require('../src/controller/Password.php');
        setPassword();
        break;
    case 'add-user':
        require('../src/controller/createUser.php');
        showForm();
        break;
    case 'save-user':
        require('../src/controller/createUser.php');
        setUser();
        break;
    case 'users-list':
        require('../src/controller/userList.php');
        break;
    case 'modify-user':
        require('../src/controller/modifyUser.php');
        modifyUser();
        break;
    case 'delete-user':
        require('../src/controller/deleteUser.php');
        deleteUser();
        break;
    case 'update-user':
        require('../src/controller/updateUser.php');
        updateUser();
        break;
    case 'images-list':
        require('../src/controller/gestioneImmagini.php');
        imagesList();
        break;
    case 'add-image':
        require('../src/controller/addImage.php');
        addImage();
        break;
    case 'save-image':
        require('../src/controller/saveImage.php');
        saveImage();
        break;
    case 'open-image':
        require('../src/controller/openImage.php');
        openImage();
        break;
    case 'edit-image':
        require('../src/controller/editImage.php');
        editImage();
        break;
    case 'upadte-image':
        require('../src/controller/updateImage.php');
        updateImage();
        break;
    case 'delete-image':
        require('../src/controller/deleteImage.php');
        deleteImage();
        break;
}